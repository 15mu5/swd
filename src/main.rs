//! `swd` stands for shorten working directory and this small application does exactly this:
//! Removing all non-essential trailing chars from path elements while retaining just as much to
//! keep them distinguishable to other sub-directories in the respective directory. Meanwhile the
//! last element is always kept as is.
//!
//! Consider the following directory tree:
//!
//! ```text
//! .
//! |-- dir1
//! |-- dir2
//!     |-- afolder
//!     |-- another_folder
//!         |-- subdir1    <-- You are here
//!             |-- test.txt
//! ```
//!
//! A call to `pwd` would thus return:
//!
//! ```text
//! $ pwd
//! /dir2/another_folder/subdir1
//! ```
//!
//! Using `swd` and give the return value of `pwd` as only argument, you get:
//!
//! ```text
//! $ swd `pwd`
//! /dir2/an/subdir1
//! ```

use std::env;
use std::io::Result;
use std::path::Path;

fn main() -> Result<()> {
    let args = env::args();
    if args.len() <= 1 {
        panic!("No path provided!");
    }
    let arg = env::args().skip(1).collect::<Vec<String>>().join(" ");
    let path = Path::new(&arg);
    if path.is_relative() {
        panic!("Path must be absolute");
    }

    if !path.is_dir() {
        panic!("Path must point to directory");
    }

    if path.parent().is_none() {
        println!("/");
        Ok(())
    } else if path.parent().unwrap().parent().is_none() {
        println!("/{}", path.file_name().unwrap().to_str().unwrap());
        Ok(())
    } else {
        let ancestors = path.ancestors().skip(2);

        let last_elem = path.file_name().unwrap().to_str().unwrap().to_owned();
        let mut parent_path = path
            .parent()
            .unwrap()
            .file_name()
            .unwrap()
            .to_str()
            .unwrap();
        let mut shortened_path_elems = vec![last_elem, "/".to_string()];

        for a in ancestors {
            let mut dirs: Vec<String> = Vec::new();
            for entry in a.read_dir()? {
                if let Ok(entry) = entry {
                    if let Ok(ft) = entry.file_type() {
                        if ft.is_dir() && entry.file_name().into_string().unwrap() != parent_path {
                            dirs.push(entry.file_name().into_string().unwrap())
                        }
                    }
                }
            }
            shortened_path_elems.push(find_shortest_unique_substring(parent_path, dirs, 1));
            shortened_path_elems.push("/".to_string());
            if let Some(f) = &a.file_name() {
                parent_path = f.to_str().unwrap()
            }
        }
        shortened_path_elems.reverse();
        println!("{}", shortened_path_elems.join(""));
        Ok(())
    }
}

fn find_shortest_unique_substring(st: &str, strs: Vec<String>, offset: usize) -> String {
    let strs = strs
        .into_iter()
        .filter(|s| {
            s.chars().take(offset).collect::<String>()
                == st.chars().take(offset).collect::<String>()
        })
        .collect::<Vec<String>>();
    if !strs.is_empty() && offset < st.len() {
        find_shortest_unique_substring(st, strs, offset + 1)
    } else {
        st[0..offset].to_string()
    }
}
