# swd

[API Documentation](https://15mu5.gitlab.io/swd/swd)

`swd` stands for shorten working directory and this small application
does exactly this: Removing all non-essential trailing chars from path
elements while retaining just as much to keep them distinguishable to
other sub-directories in the respective directory. Meanwhile the last element
is always kept as is.

Consider the following directory tree:

```
.
|-- dir1
|-- dir2
    |-- afolder
    |-- another_folder
        |-- subdir1    <-- You are here
            |-- test.txt
```

A call to `pwd` would thus return:

```
$ pwd
/dir2/another_folder/subdir1
```

Using `swd` and give the return value of `pwd` as only argument, you get:

```
$ swd `pwd`
/dir2/an/subdir1
```
